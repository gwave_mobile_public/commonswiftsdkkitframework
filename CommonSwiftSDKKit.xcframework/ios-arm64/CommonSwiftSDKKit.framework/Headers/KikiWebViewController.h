//
//  KikiWebViewController.h
//  CommonSwiftSDKKit
//
//  Created by 杨鹏 on 2024/11/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KikiWebViewController : UIViewController

- (instancetype)initWithWebviewUrl:(NSString *)webviewUrl
                         sessionId:(NSString *)sessionId
                         jwtToken:(NSString *)jwtToken
                       expandData:(NSString *)expandData;

@end

NS_ASSUME_NONNULL_END
